"{{{1 
scriptencoding utf-8
execute frawor#Setup('0.0', {'@/fwc': '0.0',
            \           '@/commands': '0.0',
            \          '@/functions': '0.0',})
call map(['cmd', 'comp'], 'extend(s:F, {v:val : {}})')
lockvar 1 s:F
let s:zefunc={}
let s:zecomp=['+ first (in *F.comp._completeEopt(@), path w)']
call s:_f.command.add('ZEdit', s:zefunc, {'nargs': '+', 'bang': 1,
            \                             'complete': s:zecomp})
let s:zvfunc={}
let s:zvcomp=['+ first (in *F.comp._completeEopt(@), path)']
call s:_f.command.add('ZView', s:zvfunc, {'nargs': '+', 'bang': 1,
            \                             'complete': s:zvcomp})
"{{{1 Функции
"{{{2 cmd, z?func
"{{{3 cmd.main
function s:F.cmd.main(action, bang, ...)
    let options=filter(copy(a:000), 'v:val[0:1]==#"++"')
    let files=filter(copy(a:000), 'v:val[0:1]!=#"++"')
    let nfiles=[]
    for file in files
        execute a:action.((a:bang)?('!'):('')).' '.
                    \join(options).' '.
                    \fnameescape(file)
    endfor
endfunction
"{{{3 zefunc
function s:zefunc.function(...)
    return call(s:F.cmd.main, ['edit']+a:000, {})
endfunction
"{{{3 zvfunc
function s:zvfunc.function(...)
    return call(s:F.cmd.main, ['view']+a:000, {})
endfunction
"{{{2 comp: _edit_complete
"{{{3 comp._completeEopt
"{{{4 Globals
let s:ppopt=['ff', 'fileformat', 'enc', 'encoding', 'bin', 'binary',
            \'nobin', 'nobinary', 'bad'] " 'edit'
let s:bads=['keep', 'drop', '?']
let s:fileformats=['dos', 'unix', 'mac']
let s:encodings=['latin1', 'koi8-r', 'koi8-u', 'macroman',
            \'cp437', 'cp737', 'cp775', 'cp850', 'cp852', 'cp855', 'cp857',
            \'cp860', 'cp861', 'cp862', 'cp863', 'cp865', 'cp866', 'cp869',
            \'cp874', 'cp1250', 'cp1251', 'cp1253', 'cp1254', 'cp1255',
            \'cp1256', 'cp1257', 'cp1258']
call extend(s:encodings, map(range(2, 15), '"iso-8859-".v:val'))
call extend(s:encodings, map(copy(s:encodings), '"8bit-".v:val'))
let s:dbencodings=['cp932', 'euc-jp', 'sjis', 'cp949',
            \'euc-kr', 'cp936', 'euc-cn', 'cp950', 'big5', 'euc-tw',
            \'japan', 'korea', 'prc', 'chinese', 'taiwan']
call extend(s:encodings, s:dbencodings)
call extend(s:encodings, map(copy(s:dbencodings),
            \'"2byte-".v:val'))
unlet s:dbencodings
call extend(s:encodings, ['utf-8', 'ucs-2', 'ucs-2le', 'utf-16',
            \'utf-16le', 'ucs-4', 'ucs-4le', 'utf8', 'unicode', 'uncs2be',
            \'ucs-2be', 'ucs-4be', 'utf-32', 'utf-32le', 'default'])
let s:ppabbr={
            \   'ff': 'fileformat',
            \  'enc': 'encoding',
            \  'bin':   'binary',
            \'nobin': 'nobinary',
        \}
let s:alts=copy(s:ppabbr)
call map(copy(s:ppabbr), 'extend(s:alts, {v:val : v:key})')
let s:withoption={
            \'ff':  'fileformats', 'fileformat': 'fileformats',
            \'enc': 'encodings',     'encoding':   'encodings',
            \'bad': 'bads',
        \}
"{{{4 comp.toarglead
let s:filterexprs=[
            \'v:val[:(larglead)] is# a:arglead',
            \'v:val[:(larglead)] ==? a:arglead',
            \'stridx(v:val, a:arglead)!=-1',
            \'v:val=~?"\\V".escape(a:arglead, "\\")',
        \]
function s:F.comp.toarglead(arglead, list)
    let larglead=len(a:arglead)-1
    for expr in s:filterexprs
        let r=filter(copy(a:list), expr)
        if !empty(r)
            return r
        endif
    endfor
    return []
endfunction
"}}}4
function s:F.comp._completeEopt(arglead)
    let start=a:arglead
    if start[0:1]==#'++'
        let start=start[2:]
        if start!~#'='
            let starts=s:F.comp.toarglead(start, s:ppopt)
            return map(starts, '"++".v:val')
        else
            let end=matchstr(start, '=\@<=.*$')
            let s=matchstr(start, '^.\{-}=\@=')
            if has_key(s:withoption, s)
                let opts=s:F.comp.toarglead(end, s:{s:withoption[s]})
                return map(opts, '"++".s."=".v:val')
            else
                let starts=s:F.comp.toarglead(s, s:ppopt)
                let alts={}
                let r=[]
                let r2=[]
                for start in starts
                    if has_key(ppabbrs, start)
                        continue
                    endif
                    if has_key(s:alts, start)
                        let alts[s:alts[start]]=1
                    endif
                    if !has_key(s:withoption, start)
                        let r2+=['++'.start]
                        continue
                    endif
                    let r+=s:F.comp._completeEopt('++'.start.'='.end)
                endfor
                if empty(r)
                    let r=r2
                endif
                return r
            endif
        endif
    endif
    return []
endfunction
"{{{1
execute frawor#Lockvar(s:, '_pluginloaded')
" vim: ft=vim:ts=8:fdm=marker:fenc=utf-8
